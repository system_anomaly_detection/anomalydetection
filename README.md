# Anomaly detection 

* Selezione dei sensori più rilevanti: K-means with DTW,soft-DTW e distanza euclidea. Valutazione con Adjusted Rand Index, Precision Recall F1-score. 
* Modelli per Anomaly-detection (unsupervised):
* Dynamicbaseline (confronto con dati storici /benchmark)
* Prophet (modello additivo trend+stagionalità, anomalie basate sull’errore di previsione)
* Random Cut Forest (decision tree) isolamento dei punti anomali
* Valutazione delle performance:
  * Precision (point adjusted evaluation), tempo medio per la detenzione
  * Precision recall (revised point adjusted), tempo medio per la detenzione
  * Conteggio delle false anomalie.
 



